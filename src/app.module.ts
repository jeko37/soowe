import { Module, NestModule } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule} from '@nestjs/mongoose'
import { ConfigModule } from './config/config.module';
import { ConfigService } from './config/config.service';
import { Configuration } from './config/config.keys';
import { UserModule } from './user/user.module';
import { AuthModule } from './auth/auth.module';
import { GraphQLModule } from '@nestjs/graphql';
import { ApolloDriverConfig, ApolloDriver } from '@nestjs/apollo';
import { CatsModule } from './cats/cats.module';

@Module({
  imports: [
    MongooseModule.forRoot(ConfigService.getEnv(Configuration.DB)), 
    ConfigModule, 
    UserModule, 
    AuthModule,
  GraphQLModule.forRoot<ApolloDriverConfig>({
    driver: ApolloDriver,
    autoSchemaFile: ConfigService.getEnv(Configuration.GRAPHQL_SCHEMA),
    debug: Boolean(ConfigService.getEnv(Configuration.GRAPHQL_DEBUG)),
    playground: Boolean(ConfigService.getEnv(Configuration.GRAPHQL_PLAYGROUND))
  }),
  CatsModule],
  controllers: [AppController],
  providers: [AppService, ConfigService],
})
export class AppModule{
  static port: number | string

  constructor(private readonly _configService: ConfigService) {
    AppModule.port = this._configService.get(Configuration.PORT)
  }
}