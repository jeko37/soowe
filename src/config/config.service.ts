import * as fs from 'fs'
import { parse } from 'dotenv'
import { Configuration } from './config.keys'

export class ConfigService {
    private envConfig: { [key: string]: string }

    constructor() {
        this.envConfig = ConfigService.verifier()
    }

    static verifier(): { [key: string]: string} {
        const isDevelopmentEnv = process.env.NODE_ENV !== 'production'

        if(isDevelopmentEnv) {
            const envFilePath = __dirname + '/../../.env'
            const existPath = fs.existsSync(envFilePath)

            if(!existPath)
            {
                console.log('.env file doesnt exist!');
                process.exit(0);
            }

            return parse(fs.readFileSync(envFilePath))
        }

        return {
            PORT: '',
            DB: ''
        }
    }

    static getDB(): string {
        return ConfigService.verifier()[Configuration.DB]
    }

    static getEnv(key: string): string {
        return ConfigService.verifier()[key]
    }

    get(key: string): string {
        return this.envConfig[key]
    }
}