import { Controller, Get, Post, Put, Delete, Res, HttpStatus, Body, Request, UseGuards } from '@nestjs/common';
import { CreateUserDTO } from './dto/user.dto';
import { UserService } from './user.service';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';

@Controller('user')
export class UserController {

    constructor(private userService: UserService) {}

    @Post('/create')
    async createUser(@Res() res, @Body() createUserDTO: CreateUserDTO) {
        const user = await this.userService.createUser(createUserDTO)

        return res.status(HttpStatus.OK).json({
            message: 'user successfully created',
            user: user
        })
    } 

    @Get('/')
    async getUsers(@Res() res) {
        const users = await this.userService.getUsers()

        return res.status(HttpStatus.OK).json({
            users
        })
    }

    @UseGuards(JwtAuthGuard)
    @Get('/delete/:id')
    async deleteUser(@Res() res, @Request() req) {
        const user = await this.userService.deleteUser(req.params.id)
        
        if(!user) {
            return res.status(HttpStatus.OK).json({
                message: 'the user has not been found'
            })
        }
        
        return res.status(HttpStatus.OK).json({
            message: 'the user has been deleted',
            user: user
        })
    }
}
