export class CreateUserDTO {
    readonly name: string
    username: string
    readonly email: string
    password: string
    readonly avatar: string
    readonly createdAt: Date
}