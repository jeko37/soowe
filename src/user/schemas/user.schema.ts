import { Schema } from "mongoose";

export const UserSchema = new Schema({
    name: {type: String, required: true},
    username: {type: String, required: true, unique: true},
    email: {type: String, required: true, unique: true},
    password: {type: String, required: true},
    avatar: {
        type: String,
        default: '/assets/images/default.png'
    },
    createdAt: {
        type: Date,
        default: Date.now
    }
})