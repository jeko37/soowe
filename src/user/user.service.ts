import { ConflictException, Injectable } from '@nestjs/common';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';
import * as bcrypt from 'bcrypt';

import { User } from './interfaces/user.interface';
import { CreateUserDTO } from './dto/user.dto';

@Injectable()
export class UserService {

    constructor(@InjectModel('User') private readonly userModel: Model<User>) { }

    async getUsers(): Promise<User[]> {
        const users = await this.userModel.find()
        return users
    }

    async getUser(userID: string): Promise<User> {
        const user = await this.userModel.findById(userID)
        return user
    }

    async createUser(createUserDTO: CreateUserDTO): Promise<User> {
        const { username, password } = createUserDTO
        const salt = await bcrypt.genSalt();

        const userExists = await this.findOne(username)
        if(userExists) {
            throw new ConflictException('username already exists');
        }

        const user = new this.userModel(createUserDTO)
        user.username = username
        user.password = await this.hashPassword(password, salt)

        await user.save();
        return user
    }

    async deleteUser(userID: string): Promise<User> {
        const deletedUser = await this.userModel.findByIdAndDelete(userID)
        return deletedUser
    }

    async updateUser(userID: string, createUserDTO: CreateUserDTO): Promise<User> {
        return await this.userModel.findByIdAndUpdate(userID, createUserDTO, {new: true})
    }

    async findOne(username: string): Promise<any> {
        return await this.userModel.findOne({ username: username })
    }

    private async hashPassword(password: string, salt: string): Promise<string> {
        return await bcrypt.hash(password, salt)
    }
}