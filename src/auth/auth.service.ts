import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserService } from '../user/user.service';
import * as bcrypt from 'bcrypt';

@Injectable()
export class AuthService {
    constructor(
        private userService: UserService,
        private jwtService: JwtService
    ) {}

    async validateUser(username: string, password: string): Promise<any> {
        const user = await this.userService.findOne(username)

        if(user && await bcrypt.compare(password, user.password)) {
            return {
                _id: user._id.toString(),
                username: user.username,
                name: user.name,
                email: user.email
            }
        }

        return null
    }

    async login(user: any) {
        const payload = { username: user.username, _id: user._id, name: user.name, email: user.email }
        console.log(payload);
        return {
            access_token: this.jwtService.sign(payload)
        }
    }
}