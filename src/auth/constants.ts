import { ConfigService } from '../config/config.service';
import { Configuration } from 'src/config/config.keys';

let _configService = new ConfigService()

export const jwtConstants = {
    secret: _configService.get(Configuration.JWT_SECRET),
    expire: _configService.get(Configuration.JWT_EXPIRE_TOKEN),
}